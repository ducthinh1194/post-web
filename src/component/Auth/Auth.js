import React, { useState, useEffect } from "react";
import { Avatar, Button, Grid, Typography, Container, Paper } from "@material-ui/core";
import LockoutlineIcon from '@material-ui/icons/LockOutlined'
import { GoogleLogin } from 'react-google-login';
import { gapi } from 'gapi-script'
import { useDispatch } from 'react-redux';
import { useHistory } from "react-router-dom";

import useStyles from './styles';
import Input from "./Input";
import Icon from "./Icon";
import { signin, signup } from '../../actions/auth';

const inititalState =  {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  confirmPassword: ''
}

const Auth = () => {
  const classes = useStyles();
  const [showPassword, setShowPassword] = useState(false);
  const [isSignUp, setIsSignUp] = useState(false);
  const [ formData, setFormData ] = useState(inititalState);
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    function start() {
      gapi.client.init({
        clientId: "98025781469-ukteg0i34q43puc0t3ujeegrhg8dsk0t.apps.googleusercontent.com",
        scope: 'email',
      });
    }

    gapi.load('client:auth2', start);
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();

    const redirectToHome = () => {
      history.push('/');
    }

    if(isSignUp) {
      dispatch(signup(formData, redirectToHome));
    }
    else {
      dispatch(signin(formData, redirectToHome));
    }
  }

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name] : e.target.value });
  }

  const switchMode = () => {
    setIsSignUp(!isSignUp);
    setShowPassword(false);
  }

  const googleSuccess = (res) => {
    const result = res?.profileObj;
    const token = res?.tokenId;

    try {
      dispatch({ type: 'AUTH', data: { result, token } });
      history.push('/');
    }
    catch(err) {
      console.log(err)
    }
  }

  const googleFailure = (error) => {
    console.log(error);
    console.log('Google sign in was unsuccessfull. Try agin later')
  }

  return (
    <Container component="main" maxWidth="xs">
      <Paper className={classes.paper} elevation={3}>
        <Avatar className={classes.avatar}>
          <LockoutlineIcon />
        </Avatar>
        <Typography variant="h5">{isSignUp ? 'Sign up' : 'Sign in'}</Typography>
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            {isSignUp && (
              <>
                <Input name="firstName" label="First Name" handleChange={handleChange} autoFocus half />
                <Input name="lastName" label="Last Name" handleChange={handleChange} half />
              </>
            )}
            <Input name="email" label="Email Address" handleChange={handleChange} type="email" />
            <Input
              name="password"
              label="Password"
              handleChange={handleChange}
              handleShowPassword={() => setShowPassword(!showPassword)}
              type={showPassword ? 'text' : 'password'} />
            {isSignUp && <Input name="confirmPassword" label="Repeat password" handleChange={handleChange} type="password" />}
          </Grid>
          <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
            {isSignUp ? 'Sign Up' : 'Sign In'}
          </Button>
          <GoogleLogin
            onSuccess={googleSuccess}
            onFailure={googleFailure}
            cookiePolicy='single_host_origin'
            clientId="98025781469-ukteg0i34q43puc0t3ujeegrhg8dsk0t.apps.googleusercontent.com"
            render={(renderProps) => (
              <Button
                disabled={renderProps.disabled}
                className={classes.googleButton}
                color="primary"
                fullWidth
                startIcon={<Icon />}
                onClick={renderProps.onClick}>
                  Google Sign In
              </Button>
            )} />
          <Grid container justifyContent="flex-end">
            <Grid item>
              <Button onClick={() => switchMode()}>
                {isSignUp ? "Already have an account? Sign In" : "Don't have an account? Sign Up"}
              </Button>
            </Grid>
          </Grid>
        </form>
      </Paper>
    </Container>
  )
}

export default Auth;