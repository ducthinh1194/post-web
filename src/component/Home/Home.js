import React, { useState, useEffect } from "react";
import { Container, Grow, Grid, Paper, AppBar, TextField, Button } from "@material-ui/core";
import { useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import ChipInput from 'material-ui-chip-input';

import Posts from "../Posts/Posts";
import Form from "../Form/Form";
import { getPosts, getPostsBySearch } from "../../actions/posts";
import Pagination from "../Pagination";
import useStyles from './styles';

const Home = () => {
  const dispatch = useDispatch();
  const [currentId, setCurrentId] = useState(null);
  const query = new URLSearchParams(useLocation().search);
  const history = useHistory();
  const page = Number(query.get('page')) || 1;
  const searchQuery = query.get('searchQuery');
  const classes = useStyles();
  const [search, setSearch] = useState('');
  const [tags, setTags] = useState([]);


  const handleKeyPress = (e) => {
    if (e.keyCode === 13) {
      // search post
      searchPost()
    }
  }

  const handleAdd = (tag) => setTags([...tags, tag]);
  const handleDelete = (tag) => setTags(tags.filter((item) => item !== tag));
  const searchPost = () => {
    if (search.trim() || tags) {
      // fetch search post
      dispatch(getPostsBySearch({ search, tags: tags.join(',') }));
      history.push(`/posts/search?searchQuery=${search || 'none'}&tags=${tags.join(',')}`);
    }
    else {
      history.push('/');
    }
  }

  return (
    <Grow in>
      <Container maxWidth='xl'>
        <Grid container justifyContent='space-between' alignItems='stretch' spacing={3} className={classes.gridContainer}>
          <Grid item xs={12} sm={6} md={9}>
            <Posts setCurrentId={setCurrentId} />
          </Grid>
          <Grid item xs={12} sm={6} md={3}>
            <AppBar className={classes.appBarSearch} position='static' color="inherit">
              <TextField
                name="search"
                variant="outlined"
                label="Search memories"
                onKeyDown={handleKeyPress}
                fullWidth
                value={search}
                onChange={(e) => { setSearch(e.target.value) }} />
              <ChipInput
                onAdd={handleAdd}
                onDelete={handleDelete}
                value={tags}
                variant="outlined"
                label="Search Tags"
                style={{
                  margin: '10px 0'
                }} />
              <Button color='primary' variant="contained" onClick={searchPost}>Search</Button>
            </AppBar>
            <Form currentId={currentId} setCurrentId={setCurrentId} />
            {(!searchQuery && !tags.length) && (
              <Paper className={classes.pagination} elevation={6}>
                <Pagination page={page} />
              </Paper>
            )}
          </Grid>
        </Grid>
      </Container>
    </Grow>
  )
}

export default Home;