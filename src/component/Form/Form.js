import React, { useEffect, useState } from 'react';
import { Button, Paper, TextField, Typography } from '@material-ui/core'
import FileBase from 'react-file-base64';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';

import useStyles from './styles';
import { createPost, updatePost } from '../../actions/posts';

const Form = ({ currentId, setCurrentId }) => {
  const classes = useStyles();
  const [postData, setPostData] = useState({
    title: '',
    message: '',
    tags: '',
    selectedFile: ''
  })
  const user = JSON.parse(localStorage.getItem("profile"));

  const post = useSelector((state) => currentId ? state.posts.posts.find(p => p._id === currentId) : null);
  const dispatch = useDispatch();

  useEffect(() => {
    if(post) setPostData(post);
  }, [post]);

  const handleSubmit = (e) => {
    e.preventDefault();

    if (currentId) {
      dispatch(updatePost(currentId, { ...postData, name:  user?.result?.name }));
    }
    else {
      dispatch(createPost({ ...postData, name:  user?.result?.name }));
    }

    clear();
  }

  const clear = () => {
    setCurrentId(null);
    setPostData({
      title: '',
      message: '',
      tags: '',
      selectedFile: ''
    })
  }
  
  if(!user?.result?.name) {
    return (
      <Paper className={classes.paper}>
        <Typography variant='h6' align='center'>
          Please sign in to create your own and like memories
        </Typography>
      </Paper>
    )
  }

  return (
    <Paper className={classes.paper}>
      <form autoComplete='off' onSubmit={handleSubmit} noValidate className={`${classes.root} ${classes.form}`}>
        <Typography variant='h6'>{currentId ? 'Editing' : 'Creating'} a memory</Typography>
        <TextField
          value={postData.title}
          onChange={(e) => setPostData({ ...postData, title: e.target.value })}
          name='title'
          variant='outlined'
          label='Title'
          fullWidth />
        <TextField
          value={postData.message}
          onChange={(e) => setPostData({ ...postData, message: e.target.value })}
          name='message'
          variant='outlined'
          multiline
          minRows={4}
          label='Message'
          fullWidth />
        <TextField
          value={postData.tags}
          onChange={(e) => setPostData({ ...postData, tags: e.target.value.split(',') })}
          name='tags'
          variant='outlined'
          label='Tags (coma separated)'
          fullWidth />
        <div className={classes.fileInput}>
          <FileBase
            type='file'
            multiple={false}
            onDone={({base64}) => setPostData({...postData, selectedFile: base64})} />
        </div>
        <Button
          className={classes.buttonSubmit}
          variant='contained'
          fullWidth
          color='primary'
          size='large'
          type='submit'>
          Submit
        </Button>
        <Button
          variant='contained'
          fullWidth
          color='secondary'
          size='small'
          onClick={clear}
          type='button'>
          Clear
        </Button>
      </form>
    </Paper>
  );
}

export default Form;