import React, { useState, useEffect } from "react";
import { AppBar, Avatar, Button, Toolbar, Typography } from '@material-ui/core';
import { Link, useHistory, useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import decode from 'jwt-decode';

import useStyles from './styles';
import logo from '../../images/logo.png';
import { LOGOUT } from "../../constants/action-types";
import { getUser } from "../../lib/util";

const Navbar = () => {
  const classes = useStyles();
  const [ user, setUser ] = useState(getUser());
  const dispatch = useDispatch();
  const history = useHistory();
  const location = useLocation();

  const logout = () => {
    dispatch({ type: LOGOUT });
    history.push('/');
    setUser(null);
  }

  useEffect(() => {
    const token = user?.token;

    if(token) {
      const decodedToken = decode(token);
      if(decodedToken.exp * 1000 < new Date().getTime()) logout();
    }

    // JWT
    const data = localStorage.getItem('profile');
    if(data) {
      setUser(JSON.parse(data));
    }
    
  }, [location]);

  return (
    <AppBar className={classes.appBar} position='static' color='inherit'>
      <div className={classes.brandContainer}>
        <img className={classes.image} src={logo} width='120' alt='memories' height='60' />
      </div>
      <Toolbar className={classes.toolbar}>
        {user ? (
          <div className={classes.profile}>
            <Avatar
              className={classes.purple}
              alt={user.result.name}
              src={user.result.imageUrl}>{user.result.name.charAt(0)}
            </Avatar>
            <Typography className={classes.userName} variant="h6">{user.result.name}</Typography>
            <Button onClick={logout} variant="contained" className={classes.logout} color="secondary">Logout</Button>
          </div>
        ) : (
          <Button component={Link} to="/auth" variant="contained" color="primary">Sign in</Button>
        )}
      </Toolbar>
    </AppBar>
  )
}

export default Navbar;