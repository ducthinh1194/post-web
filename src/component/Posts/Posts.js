import React from 'react';
import { useSelector } from 'react-redux';
import { Grid, CircularProgress } from '@material-ui/core';

import Post from './Post/Post';
import useStyles from './styles';

const Posts = ({ setCurrentId }) => {
  const {posts, isLoading} = useSelector((state) => state.posts)
  const classes = useStyles();

  if(!posts.length && !isLoading) return 'No posts';

  return (
    isLoading ? <CircularProgress /> : (
      <Grid spacing={3} container alignItems='stretch' className={classes.mainContainer}>
        {posts.map((post) => (
          <Grid key={post._id} item xs={12} sm={6}>
            <Post setCurrentId={setCurrentId} post={post} />
          </Grid>
        ))}
      </Grid>
    )
  );
}

export default Posts;