import axios from 'axios';
import { getUser } from '../lib/util';

const API = axios.create({ baseURL: 'http://localhost:5000' });
const POST_ROUTE = "/posts";
const USER_ROUTE = "/user";

API.interceptors.request.use((req) => {
  if(localStorage.getItem('profile')) {
    const user = getUser();
    req.headers.Authorization = `Bearer ${user.token}`;
  }

  return req;
})

export const fetchPosts = (page) => API.get(`${POST_ROUTE}?page=${page}`);
export const fetchPostsBySearch = (searchQuery) => API.get(`/posts/search?searchQuery=${searchQuery.search || 'none'}&tags=${searchQuery.tags}`)
export const createPost = (newPost) => API.post(POST_ROUTE, newPost);
export const updatePost = (id, updatePost) => API.patch(`${POST_ROUTE}/${id}`, updatePost);
export const deletePost = (id) => API.delete(`${POST_ROUTE}/${id}`);
export const likePost = (id) => API.patch(`${POST_ROUTE}/${id}/likePost`);
export const fetchPost = (id) => API.get(`${POST_ROUTE}/${id}`);

export const signin = (data) => API.post(`${USER_ROUTE}/signin`, data);
export const signup = (data) => API.post(`${USER_ROUTE}/signup`, data);