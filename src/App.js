import React from 'react';
import { Container } from '@material-ui/core';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Navbar from './component/Navbar/Navbar';
import Home from './component/Home/Home';
import Auth from './component/Auth/Auth';
import PostDetail from './component/PostDetail/PostDetail';
import { getUser } from './lib/util';

const App = () => {
  const user = getUser();

  return (
    <BrowserRouter>
      <Container maxWidth='xl'>
        <Navbar />
        <Switch>
          <Route path='/' exact component={() => <Redirect to="/posts" />} />
          <Route path="/posts" excat component={Home} />
          <Route path="/posts/search" excat component={Home} />
          <Route path="/post/:id" component={PostDetail} />
          <Route path='/auth' exact component={() => !user || user == null ? <Auth /> : <Redirect to="/posts" />} />
        </Switch>
      </Container>
    </BrowserRouter>
  )
}

export default App;