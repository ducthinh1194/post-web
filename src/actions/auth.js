import * as api from '../api';
import { AUTH } from '../constants/action-types';

export const signin = (formData, callBack) => async(dispatch) => {
  try {
    const { data } = await api.signin(formData);
    dispatch({ type: AUTH, data });
    callBack();
  }
  catch(err) {
    console.log(err);
  }
}

export const signup = (formData, callBack) => async (dispatch) => {
  try {
    const { data } = await api.signup(formData);
    dispatch({ type: AUTH, data });
    callBack();
  }
  catch(err) {
    console.log(err);
  }
}